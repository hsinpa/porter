﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using FacebookManager;

public class InGameEventHandler : MonoBehaviour {
	private facebookMain facebookMain;
	private bool menuStatus = false;
	
	public static bool gameOver;
	public 	Animator anim;
	
	void Start() {
		facebookMain = gameObject.AddComponent<facebookMain>();
		anim = GetComponent<Animator> ();
		gameOver = false;
		FB.Init(SetInit, OnHideUnity);		
	}
	
	public void reStart()  {
		Application.LoadLevel("gameScene");
		AdsManager.hideBanner();
	}
	
	public void leaveGame() {
		Application.LoadLevel("menuScene");
		AdsManager.hideBanner();
	}
	
	public void shareToFB() {
		if(FB.IsLoggedIn) {
			FB.Feed(
				link: "http://hsinpa.com/",
				linkName: "Porter",
				linkCaption: GameOverManager.overAllScore +" is make in single-player round",
				linkDescription: "LEave to google play",
				callback: FBShareCallback
				);
		} else {
			facebookMain.facebookLogin();
		}
	}
	
	public void menuAction() {
	if (!gameOver) {
			if (!menuStatus) {
				
				Time.timeScale = (CubeMap.multiplayerMode) ? 1 : 0;
				menuStatus = true;
				anim.SetBool("isHidden", false);
				
			} else {
				Time.timeScale = 1;
				menuStatus = false;
				anim.SetBool("isHidden", true);
				
			}
		}
	}
	private void FBShareCallback(FBResult response) {
		Debug.Log(response.Text);
	}
	
	private void SetInit() {
		enabled = true; 
		// "enabled" is a magic global; this lets us wait for FB before we start rendering
	}
	
	private void OnHideUnity(bool isGameShown) {
		if (!isGameShown) {
			// pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// start the game back up - we're getting focus again
			Time.timeScale = 1;
		}
	}
}
