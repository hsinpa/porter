﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class GameOverManager : MonoBehaviour {
	
	Text[] resultArray;
	public static float overAllScore;
	
	void Start() {
		overAllScore = 0;
	}
	
	public void gameOver() {
	//1 = score, 0 = time
		GameObject.Find("SideMenuPanel").SetActive(false);
		gameObject.SetActive(true);
		resultArray = transform.GetComponentsInChildren<Text>();
	
		Time.timeScale = 0;
		overAllScore = getOverAllScore();
		resultArray[0].text = resultArray[0].text + " : "+ (Time.time - CubeMap.start_time) +" s";
		resultArray[1].text = resultArray[1].text + " : "+ScoreManager.score;
		resultArray[2].text = resultArray[2].text + overAllScore;
		
		InGameEventHandler.gameOver = true;
		if (!CubeMap.multiplayerMode) {
			string game_id = PlayerPrefs.GetString("facebookId", "");
			string name = PlayerPrefs.GetString("name", "Anonymous");
			JSONObject form = new JSONObject();
			form.AddField("facebook_id", game_id);
			form.AddField("name", name);
			form.AddField("character", CubeMap.characterPrefab);
			form.AddField("stage", CubeMap.themePrefab);
			form.AddField("overAllScore", overAllScore);
			saveRecord( form );
		} else {
			GameObject.Find("SocketIO").GetComponent<SocketIO_Room>().socket.Close();
		}
		AdsManager.showBanner();
	}
	
	//saveRecord only in single player
	private void saveRecord(JSONObject json) {
		String matchData = PlayerPrefs.GetString("match"+json.GetField("stage").str, "");
		if (!matchData.Equals("") && !matchData.Equals("null")) {
			JSONObject matchJSON = new JSONObject(matchData);
			//Save new record if score larger
			if (matchJSON.GetField("overAllScore").n < json.GetField("overAllScore").n) {
				PlayerPrefs.SetString("match"+json.GetField("stage").str, json.ToString());
				
			sendToServer(json);
			}
		} else {
			PlayerPrefs.SetString("match"+json.GetField("stage").str, json.ToString());
			sendToServer(json);
		}
		PlayerPrefs.Save();
	}
	
	//Parse to WWWForm
	private void sendToServer(JSONObject obj ) {
		WWWForm form = new WWWForm();
		List<string> keys = obj.keys;
		for(int i = 0; i < obj.list.Count; i++) {
			string key = keys[i];
			string val = obj.list[i].ToString();
			val = val.Replace("\"", "");
			form.AddField(key, val);
		}
		WWW www = new WWW(CubeMap.baseUrl + "saveRecord", form);
	}
	
	public static float getOverAllScore() {
		return Mathf.Round(((ScoreManager.score / 3) + (Time.time - CubeMap.start_time) / 7) / ((ScoreManager.fireballhit_amount *0.1f)+1) * 100) / 100;
	}
}

