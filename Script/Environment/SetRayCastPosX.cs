﻿using UnityEngine;
using System.Collections;

public class SetRayCastPosX : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		WithForeachLoop ();
	}

	//Set the position for other detector
	void WithForeachLoop()
	{
		float cubeNum = transform.position.x;
		foreach (Transform child in transform) {
			child.position = new Vector2 (cubeNum, child.position.y);
			cubeNum++;
		}
	}
	
}
