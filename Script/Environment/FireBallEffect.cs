﻿using UnityEngine;
using System.Collections;

public class FireBallEffect : MonoBehaviour {
	
	public Transform dropEffect;
	public float stun = 2;
	//public float slowSpeed = 1.5f;
	
	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.transform.tag == "Player") {
			Instantiate(dropEffect, transform.position, dropEffect.rotation);
			PlayerController player = collision.transform.GetComponent<PlayerController>();
			ScoreManager.fireballhit_amount++;
			player.DoCoroutine(stun);
			player.soundManager.playSound("explosion");
			Vibrate.vibrate(200);
		}
		Destroy(gameObject);
	}
}
