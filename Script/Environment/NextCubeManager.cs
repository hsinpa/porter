﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NextCubeManager : MonoBehaviour {

	RawImage nextCube;
	public Texture2D[] cubeTexute;
	
	// Use this for initialization
	void Start () {
		nextCube = GetComponent<RawImage>();
	}
	
	public void changeCubeImage(int index) {
		nextCube.texture = cubeTexute[index];
	}
}
