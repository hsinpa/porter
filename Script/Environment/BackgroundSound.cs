﻿using UnityEngine;
using System.Collections;

public class BackgroundSound : MonoBehaviour {
	public AudioClip[] themeMusic;
	private AudioSource audioSource;
	
	// Use this for initialization
	void Start () {
	int themeName = (CubeMap.themePrefab.Equals("Day_Theme")) ? 0 : 1;
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = themeMusic[themeName];
		audioSource.Play();
	}
}
