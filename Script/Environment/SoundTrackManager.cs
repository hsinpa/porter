﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundTrackManager : MonoBehaviour {

	public AudioSource audioSource;
	Dictionary<string, AudioClip> audioClips = new Dictionary<string, AudioClip>();
	private string[] movementAudio = {"footstep" , "rotateCube", "drop", "clearChain", "explosion"};
	
	// Use this for initialization
	void Start () {
		gatherSound();
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	private void gatherSound() {
		for (int i = 0; i < movementAudio.Length; i++ ) {
			audioClips.Add(movementAudio[i], Resources.Load<AudioClip>("MovementMusic/" + movementAudio[i]) );
		}
	}
	
	public void playSound(string soundName) {
		audioSource.clip = audioClips[soundName];
		audioSource.Play();
	}
}
