﻿using UnityEngine;
using System.Collections;

public class SetRayCastPosition : MonoBehaviour {
	
	public ArrayList cubeHeightArray= new ArrayList();
	private float lastCallTime = 0f;
	private SocketI0_Game socketIOGame;
	
	// Use this for initialization
	void Start () {
		WithForeachLoop ();
		if (CubeMap.multiplayerMode) {
			socketIOGame = GameObject.Find ("SocketIO").GetComponent<SocketI0_Game>();
		}
	}
	


	//Set the position and index for other detector
	void WithForeachLoop()
	{
		int cubeIndex = 0;
		float cubeHeight = transform.position.y -1;
		foreach (Transform child in transform) {
			cubeHeight++;			
			child.position = new Vector2 (child.position.x, cubeHeight);
			child.GetComponent<RaycastY>().cubeIndex = cubeIndex;
			cubeIndex++;
		}
	}
	
	public IEnumerator updateStatus() {
		yield return new WaitForSeconds(0.1f);
		
		if (Time.time > lastCallTime && cubeHeightArray.Count > 0) {
			lastCallTime = Time.time + 0.001f;
			cubeHeightArray.Sort(new ReverseSort());
			socketIOGame.updateStatus(cubeHeightArray[0].ToString());
			cubeHeightArray.Clear();
		}
	}
	
	
	public class ReverseSort : IComparer
	{
		public int Compare(object x, object y)
		{
			// reverse the arguments
			return Comparer.Default.Compare(y, x);
		}
		
	}
}
