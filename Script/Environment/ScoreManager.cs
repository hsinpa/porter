﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class ScoreManager : MonoBehaviour {
	
	public static float score;
	public static string killText;
	
	public static int fireballhit_amount;
	public static float lastCallTime = 0;
	public static int comboTime = 0;
	private LocalizationManager locationManager;
	
	public static List<string> killSentences = new List<string>();
	private static Animator anim;
	
	private GameObject killTextObject;

	private Text chainEffect;
	private Text text;
	
	private AudioSource audioSource;
	private AudioClip chainClear;
	// Use this for initialization
	void Awake () {
	
		killTextObject = GameObject.Find("GameScene/Canvas/killSentence");
		anim = killTextObject.GetComponent<Animator>();
		chainEffect = killTextObject.GetComponent<Text>();
		audioSource = gameObject.GetComponent<AudioSource>();
		chainClear = Resources.Load<AudioClip>("MovementMusic/clearChain");
		audioSource.clip = chainClear;
		
		text = GetComponent<Text>();
		score = 0;
		fireballhit_amount = 0;
		text.text=""+ score;
	}
	
	void Start() {
		locationManager = GameObject.Find("LocalizationManager").GetComponent<LocalizationManager>();
		killSentences.Add(locationManager.GetWord("doubleChain"));
		killSentences.Add(locationManager.GetWord("tripleChain"));
		killSentences.Add(locationManager.GetWord("ultraChain"));
		killSentences.Add(locationManager.GetWord("rampage"));
	}
	
	public void addScore() {
		if (Time.time < lastCallTime) {
			comboTime++;
			score += Mathf.Floor(100 * (1.4f * comboTime));
			
			//if clear above 2 rows then play sound and anim
			if (comboTime > 1) {
				audioSource.Play();
				anim.Play("chainEffect", -1, 0);
				killText = (comboTime > 5) ? killSentences[3] : killSentences[comboTime-2];
				if (comboTime >= 3 && CubeMap.multiplayerMode) {
					GameObject.Find("SocketIO").GetComponent<SocketI0_Game>().ironCubeQueue();
				}
			}
		} else {
			score += 100;
			comboTime = 1;
			lastCallTime = Time.time + 5;
		}
		text.text=""+ score;
		chainEffect.text = killText + "!";
	}
}
