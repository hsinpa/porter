﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;
using System;

public class AdsManager : MonoBehaviour {
	private string unityAdsAndroidId = "131621884";
	private static BannerView bannerView;
	
	public void requestUnityAds() {
		if (Advertisement.isSupported) {
			Advertisement.allowPrecache = true;
			Advertisement.Initialize (unityAdsAndroidId);
		}
	}
	
	public static void RequestBanner()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3755599441373803/8211850773";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
					bannerView.AdLoaded += HandleAdLoaded;
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
	}
	
	public static void HandleAdLoaded(object sender, EventArgs args)
	{
		bannerView.Hide();
	}
	
	public static void showBanner() {
		bannerView.Show();
	}
	
	public static void hideBanner() {
		bannerView.Hide();
	}
}
