﻿using UnityEngine;
using System.Collections;
using System;

public class Vibrate : MonoBehaviour {
	#if UNITY_ANDROID
	AndroidJavaClass javaClass = null;
	AndroidJavaObject activity = null;
	#endif
	
	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		AndroidJNI.AttachCurrentThread();
		javaClass = new AndroidJavaClass("com.hsin.androidunityplugin.App");
		activity = javaClass.GetStatic<AndroidJavaObject>("mContext");
		#endif
	}

	public static void vibrate(int milisecond) {
		#if UNITY_ANDROID
		if (SystemInfo.deviceType.ToString() == "Handheld") {
			AndroidJNI.AttachCurrentThread();
			using (AndroidJavaClass javaClass = new AndroidJavaClass("com.hsin.androidunityplugin.App"))
			{
				using (AndroidJavaObject activity = javaClass.GetStatic<AndroidJavaObject>("mContext"))
				{
					activity.Call("vibrate", Convert.ToInt64(milisecond));
				}
			}
		}
		#endif
	}
}
