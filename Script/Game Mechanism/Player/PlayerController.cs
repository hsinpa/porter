﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class PlayerController : MonoBehaviour {

	GestureAction gestureAct;
	Character character;
	
	CubeMap cubemap;
	TopDetection topDetect;
	public SoundTrackManager soundManager;
	
	float floorHeight;
	float centerPoint;
	float force;
	float hitLength = 0.7f;
	int wallLayer = 1 << 0;
	int cubeLayer = 1 << 9;
	Vector2 startPosition;
	float startTime;
	delegate void UserInputMethod();
	UserInputMethod userInputMethod;
	public float moverSpeed;
	public float jumpStreght;
	public float carrySpeed;
	
	
	//Conditional Valuable
	public 	Animator anim;
	public bool onFloor = true;
	public bool carryIron = false;
	public bool carryCube = false;
	public bool isStun = false;
	//Right = 1, left = -1
	public float moveDirection = 1;
	public Vector3 renderSize;

	
	void Start () {
		floorHeight = (Screen.height * 0.14f);
		centerPoint = Screen.width / 2;
		gestureAct = GetComponent<GestureAction> ();
		anim = GetComponent<Animator> ();
		topDetect = GetComponentInChildren<TopDetection>();
		soundManager = gameObject.AddComponent<SoundTrackManager>();
		
		if (SystemInfo.deviceType.ToString() == "Desktop") {
			userInputMethod = desktopInput;
		} else {
			userInputMethod = mobileInput;
		}
		
		if (CubeMap.characterPrefab.Equals("police")) character = gameObject.AddComponent<Police>();
		if (CubeMap.characterPrefab.Equals("fireFighter"))character = gameObject.AddComponent<FireFighter>();
		if (CubeMap.characterPrefab.Equals("worker")) character = gameObject.AddComponent<Worker>();
		moverSpeed = character.speed;
		jumpStreght = character.jumpHeight;
		carrySpeed = character.carrySpeed;
	}

	void Update() {
		if (!isStun) {
			userInputMethod();
		}
	}
	
	void desktopInput() {
		if (Input.GetKey ("space") && onFloor) {
			jump ();
		} else if (Input.GetKey ("right") || Input.GetKey ("left")) {
			move (true);
		} else if (Input.GetKeyDown ("up")) {
			cubeRotate ();
		} else if (Input.GetKeyDown ("down")) {
			throwCube ();
		} else {
			anim.SetFloat("Speed", 0f);
		}
	}
	void mobileInput () {
				if (Input.GetMouseButtonDown (0) && gestureAct.doubleClick () && onFloor) {
						jump ();
				} else if (Input.GetButtonUp ("Fire1")) {
						Vector2 endPosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
						Vector2 delta = endPosition - startPosition;
			
						float dist = Mathf.Sqrt (Mathf.Pow (delta.x, 2) + Mathf.Pow (delta.y, 2));
						float angle = Mathf.Atan (delta.y / delta.x) * (180.0f / Mathf.PI);
						float duration = Time.time - startTime;
						float speed = dist / duration;
						if (angle < 0)
								angle = angle * -1.0f;
						//Debug.Log ("Distance: " + dist + " Angle: " + angle + " Speed: " + speed);

						if (startPosition.y < endPosition.y && dist > 120 && angle > 60 && speed > 400) {
								// Top swipe
								cubeRotate ();
						} else if (startPosition.y > endPosition.y && dist > 120 && angle > 50 && speed > 350) {
								// Bottom swipe
								throwCube ();
						} 

				} else if (Input.GetButton ("Fire1")) {
								move (false);
//								startPosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
//								startTime = Time.time;
				} else {
						anim.SetFloat("Speed", 0f);
		}
		
		if (Input.GetButtonDown ("Fire1")) {
						startPosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
						startTime = Time.time;
		}
	}

	
	//Move action
	void move(bool isDestop) {
		Vector2 touchDeltaPosition = Input.mousePosition;
		if ( (touchDeltaPosition.y < floorHeight  && startPosition.y < floorHeight) || isDestop)  {
			anim.SetFloat("Speed", 1f);
			float newSpeed = (carryCube) ? carrySpeed : moverSpeed;
			newSpeed = (carryIron) ? newSpeed / 1.8f : newSpeed;
			
			if ((touchDeltaPosition.x > centerPoint && !isDestop) || Input.GetKey ("right")) {
				moveDirection = 1f;
				transform.rotation = Quaternion.Euler(0, 0, 0);
			} else if ((touchDeltaPosition.x < centerPoint && !isDestop) || Input.GetKey ("left")) {
				moveDirection = -1f;
				transform.rotation = Quaternion.Euler(0, 180, 0);
			}
			//Make Sure carryCube won't rotate
			transform.GetChild(2).transform.rotation = Quaternion.Euler(0, 0, 0);
			transform.Translate ( newSpeed * Time.deltaTime, 0, 0);
		}
	}

	//============================================ Player Action =================================
	//Jump action
	void jump() {
		if ( !carryIron ) {
			float jumpHeight = (carryCube) ? jumpStreght/1.2f : jumpStreght; 
			rigidbody2D.AddForce (new Vector2 (0, jumpHeight));
			anim.SetBool("onFloor", false);
			soundManager.playSound("footstep");
			
			onFloor = false;
			
			if (rigidbody2D.velocity.magnitude > 3) {
				
				rigidbody2D.velocity = Vector2.zero;
			}
		}
	}

	//Rotate Cube 
	void cubeRotate() {
		if (carryCube) {
			gameObject.GetComponentInChildren<CubeController>().cubeRotate();
			soundManager.playSound("rotateCube");
		}
	}

	//Action to put cube down
	void throwCube() {
		if (carryCube) {
			renderSize = gameObject.GetComponentInChildren<cubeFullSize> ().getChildWidth ();
			preThrowCubeDetect (moveDirection, renderSize);
			floorDector();
		}
	}

	//============================================ Constraint Detection ================================
	//On Floor Dectection (Detect when to jump)
	public void floorDector() {
		int CombinedMask = cubeLayer | wallLayer;
		if (Physics2D.CircleCast (transform.position, 1f ,new Vector2 (0, -1f), 1.3f, CombinedMask)) {
			anim.SetBool("onFloor", true);
			onFloor = true;
		}
	}

	//Detect the cube size and distance constraint (posDir = right:1, left:0)
	void preThrowCubeDetect(float throwDirection, Vector3 renderSize) {

		bool mayIPut = courierWidth (renderSize, throwDirection);
		if (mayIPut) {
			anim.SetBool("carryCube", false);
			carryCube = false;
			carryIron = false;
			gameObject.GetComponentInChildren<CubeController> ().placeCube (renderSize, throwDirection);
			soundManager.playSound("drop");
		}
	}

	bool courierWidth(Vector3 renderSize, float throwDirection) {
		CubeDetector[] cubeDetect = gameObject.GetComponentsInChildren<CubeDetector> ();
		int CombinedMask = cubeLayer | wallLayer;
		
		for (int i  = 0; i < cubeDetect.Length; i++) {
			Vector2 cubePos = cubeDetect[i].cubePos();
			float cubePutPosX, cubePutPosY = cubePos.y - 2f;
			
			if (throwDirection == 1f) {
				cubePutPosX = cubePos.x + (renderSize.x / 2 + 0.5f);
			} else {
				cubePutPosX = cubePos.x - (renderSize.x / 2 + 0.5f);
			}
			Vector2 area1 = new Vector2(cubePutPosX - 0.05f, cubePutPosY+0.05f);
			Vector2 area2 = new Vector2(cubePutPosX + 0.05f, cubePutPosY-0.05f);
			
			Collider2D hit = Physics2D.OverlapArea(area1, area2 , CombinedMask);
			if (hit) {
				Debug.Log(hit.gameObject.name);
				return false;
			}
		}
		return true;
	}
	
	public void DoCoroutine(float stun){
		StartCoroutine(unStun(stun));
	}
	
	//UnStun
	public IEnumerator unStun(float stun) {
		isStun = true;
		yield return new WaitForSeconds(stun);
		isStun = false;
	}
	
	void OnCollisionEnter2D(Collision2D collision) {
		floorDector ();
		if (collision.gameObject.layer == 2) {
			topDetect.carry(collision.gameObject);
		}
	}
}