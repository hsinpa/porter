﻿using UnityEngine;
using System.Collections;

public class GestureAction : MonoBehaviour {
	float doubleClickStart = 0;
	Vector2 firstTouch;
	

	//Check if double click event is trigger
	public bool doubleClick() {
		if ((Time.time - doubleClickStart) < 0.3f) {
		
			if (check_clickSpace(firstTouch, Input.mousePosition)) {
				firstTouch = Vector2.zero;
				doubleClickStart = -1;
				return true;
			}
		} else {
			firstTouch = Input.mousePosition;
			doubleClickStart = Time.time;
		}
		return false;
	}
	
	public bool check_clickSpace(Vector2 firstTouch ,Vector2 secondTouch) {
		int range = 50;
		float colXmin = secondTouch.x - range,
				colXmax = secondTouch.x + range,
				colYmin = secondTouch.y - range,
				colYmax = secondTouch.y + range;
		
		if ((firstTouch.x > colXmin && firstTouch.x < colXmax) && (firstTouch.y > colYmin && firstTouch.y < colYmax)) {
			return true;
		}
		return false;
	}
}
