﻿using UnityEngine;
using System.Collections;

public interface Character  {
	float speed { get; }
	float jumpHeight { get; }
	float carrySpeed { get; }
	
}
