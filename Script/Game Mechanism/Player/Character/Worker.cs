﻿using UnityEngine;
using System.Collections;

public class Worker : MonoBehaviour, Character {
	
	public float speed { get{ return 6f; } }
	public float jumpHeight { get{ return 31f; } }
	public float carrySpeed { get{ return  5f; } }
	
}
