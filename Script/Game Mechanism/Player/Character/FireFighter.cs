﻿using UnityEngine;
using System.Collections;

public class FireFighter : MonoBehaviour, Character {

	public float speed { get{ return 6.5f; } }
	public float jumpHeight { get{ return 34.5f; } }
	public float carrySpeed { get{ return  4.2f; } } 
	
}
