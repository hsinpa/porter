﻿using UnityEngine;
using System.Collections;

public class Police : MonoBehaviour, Character {
	public float speed { get{ return 8f; } }
	public float jumpHeight { get{ return 30f; } }
	public float carrySpeed { get{ return  4f; } } 
}
