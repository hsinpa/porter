﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TopDetection : MonoBehaviour {
	PlayerController player;

	// Use this for initialization
	void Start () {
		player = transform.parent.GetComponent<PlayerController> ();
	}

	//Inject cube into player
	void OnTriggerEnter2D (Collider2D collision) {
		carry (collision.gameObject);
	}
	
	public void carry(GameObject collision) {
		if (collision.transform.tag == "Cubes" && player.carryCube == false
		    && collision.transform.root.gameObject.GetComponent<CubeController>().isLand == false) {
			transform.localPosition = new Vector2(0, transform.localPosition.y);
			
			collision.gameObject.GetComponent<CubeDetector>().CallCubeGenerator();
			
			CubeController cubes;
			cubes = collision.transform.root.gameObject.GetComponent<CubeController>();
			cubes.fixCube (false);
			cubes.parentToPlayer(transform);
			
			cubes.beCarried = true;
			cubes.isLand = true;
			
			player.carryCube = true;
			player.carryIron = (cubes.isIronCube) ? true : false;
			player.anim.SetBool("carryCube", true);
			player.soundManager.playSound("footstep");
			player.floorDector();
			//Change parent to layer 1
			changeLayer(10, collision.transform.parent.transform);
		}
	}

	//Change the layer of cube child to avoid raycast
	public bool changeLayer(int layerNum, Transform transform) {
		foreach (Transform child in transform) {
			child.gameObject.layer = layerNum;	
		}
		return true;
	}
}
