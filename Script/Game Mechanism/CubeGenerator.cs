﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeGenerator : MonoBehaviour {

	//Add Every(7) Cube gameobect to arrayList
	public static bool ironQueue;
	public List<GameObject> cubeArray= new List<GameObject>();
	private int nextCubeIndex;
	private int[] nextCubeIndexArray;//Random
	private float miniFixPos = 0f;
	private GameObject[] nextCubeManager;
	private LevelManager levelManager;
	
	public void Start() {
		nextCubeIndex = Random.Range (0, 7);
		nextCubeManager = GameObject.FindGameObjectsWithTag("nextCube");
		levelManager  = gameObject.GetComponent<LevelManager>();
		ironQueue = false;
	}

	//Generate Cube from the gernerator position and cubeArray
	//xPos is receive from CubeMap
	public void createCube(float xPos){
		StartCoroutine (waitCubeGenerate (xPos));
	}

	IEnumerator waitCubeGenerate(float xPos) {
		yield return new WaitForSeconds(2f);
		if (cubeArray [nextCubeIndex].transform.name == "Square" || cubeArray [nextCubeIndex].transform.name == "Lleft" || 
		    cubeArray [nextCubeIndex].transform.name == "Lright") {
			miniFixPos = -0.5f;
		} else {
			miniFixPos = 0f;
		}

		GameObject cube = Instantiate (cubeArray[nextCubeIndex], new Vector2 (xPos + miniFixPos, transform.position.y), transform.rotation) as GameObject;
		changeToIronSkin(cube);
		StartCoroutine(levelManager.projectTrap());
		updateNextCube(nextCubeBuilder());
	}
	
	private void updateNextCube(int[] indexArray) {
		for (int i = 0; i < nextCubeManager.Length; i++ ) {
			nextCubeManager[i].BroadcastMessage("changeCubeImage", indexArray[i], SendMessageOptions.DontRequireReceiver);
		}
		nextCubeIndex = indexArray[Random.Range(0, indexArray.Length)];
	}
	
	private int[] nextCubeBuilder() {
		int[] indexArray = new int[2];
		int nextCubeIndex1 = Random.Range (0, 7),
			nextCubeIndex2 = Random.Range (0, 7);
		indexArray[0] = nextCubeIndex1;
		indexArray[1] = nextCubeIndex2;
		return indexArray;
	}
	
	private void changeToIronSkin(GameObject cube) {
		if(ironQueue) {
			cube.GetComponent<CubeController>().isIronCube = true;
			SpriteRenderer[] ironSprite = cube.GetComponentsInChildren<SpriteRenderer>();

			for (int i = 0; i < ironSprite.Length; i++) {
				ironSprite[i].sprite = Resources.Load<Sprite>("gameSprite/ironQueue");
			}
			ironQueue = false;
		}
	}
}