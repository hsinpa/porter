﻿using UnityEngine;
using System.Collections;

public class RaycastX : MonoBehaviour {
	
	int cubeLayer = 1 << 9;
	int playerLayer = 1 << 10;
	float lastCallTime = 0f;
	
//	void Update() {
//		Debug.DrawLine(new Vector3(transform.position.x, transform.position.y +27, transform.position.z), transform.position, Color.red);
//	}
	
	
	//use to reposition the fallen and rotate cube in paralle to raycastDetectorX
	public void raycastRepositionX(bool withPlayer) {
		if (Time.time > lastCallTime) {
			lastCallTime = Time.time + 0.0001f;
			
		int CombinedMask = (withPlayer) ? cubeLayer | playerLayer : cubeLayer;
		
		RaycastHit2D[] hits;
		
			hits = Physics2D.CircleCastAll (transform.position, 0.028f, new Vector2 (0f, 1f), 27.0F, CombinedMask);
		int i = 0;
		
			while (i < hits.Length) {
				RaycastHit2D hit = hits [i];
				hit.collider.gameObject.transform.position = new Vector2(transform.position.x,
				                                                         hit.collider.gameObject.transform.position.y);
				
				i++;
			}
		}
	}
}
