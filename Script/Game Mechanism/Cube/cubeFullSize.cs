﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class cubeFullSize : MonoBehaviour {
	CubeController cubeCtrl;

	void Start() {
		cubeCtrl = transform.parent.GetComponent<CubeController>();
		InvokeRepeating("collectGarbage", 30f, 30f);
	}

	public void fixChildCube(bool forChild)	{
		foreach (Transform childs in transform) {
			childs.rigidbody2D.velocity = Vector2.zero;
			childs.rigidbody2D.isKinematic = true;
			childs.rigidbody2D.gravityScale = 35;
			childs.gameObject.layer = 9;
		}
		if (forChild) {
			cubeCtrl.reCenterPosition(true, false);
		}
	}
	
	//Calculate Child Width
	public Vector3 getChildWidth() {
		Vector3 center = Vector3.zero;
		
		foreach (Transform child in transform) {

			center += child.gameObject.renderer.bounds.center;   
			
		}
		center /= transform.childCount; //center is average center of children
		
		//Now you have a center, calculate the bounds by creating a zero sized 'Bounds', 
		Bounds bounds = new Bounds(center,Vector3.zero); 
		
		foreach (Transform child in transform)
		{
			
			bounds.Encapsulate(child.gameObject.renderer.bounds);  
			
		}
		//Debug.Log (bounds.size.ToString ());
		return bounds.size;
	}

	public List<int> getPartialWidth() {
		List<float> heightArray = new List<float> ();
		foreach (Transform child in transform) {
			heightArray.Add( Mathf.Round(child.gameObject.renderer.bounds.center.y * 100f) / 100f); 
		}

		heightArray.Sort ();

		List<int> heightSort = new List<int> ();
		float previousNum = 0f;

		for (int i = 0; i < heightArray.Count; i++) {
			int repeatNum = 0;
			for (int j = 0; j < heightArray.Count; j++) {
				if (heightArray[i] == heightArray[j]) {
					repeatNum++;
				}
			}
			//Debug.Log (heightArray[i]);

			if (heightArray[i] != previousNum) {
				heightSort.Add(repeatNum);
			}
			previousNum = heightArray[i];
		}

		return heightSort;
	}
	
	//For Garbage Collection Purpose
	public void collectGarbage() {
		if (transform.childCount == 0) {
			Destroy(transform.parent.gameObject);
		}
	}
}
