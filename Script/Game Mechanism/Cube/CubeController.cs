﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CubeController : MonoBehaviour {
	private cubeFullSize cubeFullSize;	
	private	CubeMap cubeMap;
	
	public bool isLand = false;
	public bool beCarried = false;
	public bool isIronCube = false;
	public int fallingSpeed = 8;
	
	
	public SetRayCastPosition raycastMainY;
	public CubeDetector[] childCubes;
	
	//wall and cube layer
	int wallLayer = 1 << 0;
	int cubeLayer = 1 << 9;	
	
	void Start() {
		cubeMap = GameObject.Find("CubeGenerator").gameObject.GetComponent<CubeMap>();
		cubeFullSize = gameObject.GetComponentInChildren<cubeFullSize> ();
		childCubes = transform.GetComponentsInChildren<CubeDetector>();
		setGravity();
		
		raycastMainY = GameObject.Find("GameScene/RaycastY").GetComponent<SetRayCastPosition>();
		
	}

	//Check Physical barricade before rotate
	bool rotateDetector(Vector3 cubeWidth) {
		int CombinedMask = wallLayer;

		RaycastHit2D[] hitsLeft;
		hitsLeft = Physics2D.RaycastAll (transform.position, new Vector2 (-1f, 0f), (cubeWidth.y /2) - 0.5f, CombinedMask);
		RaycastHit2D[] hitsRight;
		hitsRight = Physics2D.RaycastAll (transform.position, new Vector2 (1f, 0f), (cubeWidth.y /2) - 0.5f, CombinedMask);

		if (hitsRight.Length == 0 && hitsLeft.Length == 0) {
				return true;
			} else {
				return false;
		}
	}

	public void cubeRotate() {
		gameObject.GetComponentInChildren<cubeFullSize>().getPartialWidth();
		Vector3 cubeWidth = gameObject.GetComponentInChildren<cubeFullSize>().getChildWidth();

		if (rotateDetector(cubeWidth) && transform.name != "Square(Clone)") {
			
			transform.Rotate (new Vector3(transform.rotation.x, transform.rotation.y, Mathf.Round(90f)));			
			foreach (CubeDetector childCube in childCubes) {
				childCube.recorrectRotate();
			}
			Vector3 newCubeWidth = gameObject.GetComponentInChildren<cubeFullSize>().getChildWidth();
			transform.localPosition = new Vector2(0f, newCubeWidth.y / 2f);
		}
	}
	
	private void setGravity() {
		rigidbody2D.gravityScale = cubeMap.gravity;
		foreach (CubeDetector childCube in childCubes) {
			childCube.rigidbody2D.gravityScale = cubeMap.gravity;
		}
	}
	
	//Call RaycastPos
	public void reCenterPosition(bool isDelete, bool isPlayer) {
		callRaycastX(isPlayer);
		callRaycastY(isDelete);
		if (CubeMap.multiplayerMode) {
			StartCoroutine(raycastMainY.updateStatus());
		}
	}
	
	private void callRaycastX(bool isPlayer) {
		foreach (RaycastX cube in cubeMap.raycastXSet) {
			cube.raycastRepositionX(isPlayer);
		}
	}
	
	private void callRaycastY(bool isDelete) {
		foreach (RaycastY cube in cubeMap.raycastYSet) {
			cube.raycastYPos(isDelete);
		}
	}
	
	
	//Put the Cube Down (newDir = 0:right, 1:left)
	public void placeCube(Vector3 renderSize, float newDir) {
		beCarried = false;
		isLand = true;
		if (transform.parent.GetComponent<TopDetection>().changeLayer(9, transform.GetChild(0))) {
		//Detach parent
		
		transform.parent.transform.DetachChildren();

		Vector2 newPos;
		if (newDir == 1) {
			newPos = new Vector2 (transform.position.x + (renderSize.x/2 + 0.5f), transform.position.y - 2);

				} else {

			newPos = new Vector2 (transform.position.x - (renderSize.x/2 + 0.5f), transform.position.y - 2);
		}

		transform.position = newPos;
		reCenterPosition (false, true);
		freeFall();
		}
	}
	
	//Make the Cube iskinematic
	public void fixCube(bool forChild) {
		transform.rigidbody2D.isKinematic = true;
		cubeFullSize.fixChildCube (forChild);
		
	}
	
	
	//After kinematic, unfreeze it for the cube carry by player
	public void freeFall() {
		foreach (CubeDetector childs in childCubes) {
			childs.collider2D.isTrigger = false;
			childs.rigidbody2D.isKinematic = false;
			childs.rigidbody2D.velocity = new Vector2(0 , -9.5f);
		}
	}
	
	public void parentToPlayer(Transform parent) {
		transform.parent = parent;
		Vector3 carryPosition = cubeFullSize.getChildWidth ();
		transform.localPosition = new Vector2(0f, carryPosition.y / 2f);
		foreach (CubeDetector childCube in childCubes) {
			childCube.collider2D.isTrigger = true;
			childCube.toOriginalPos();
		}
	}
}
