﻿using UnityEngine;
using System.Collections;

public class CubeDetector : MonoBehaviour {

	CubeController cubeCtrl;
	CubeMap cubeMap;
	Vector2 originalPos;
	
	//Get all gameObject inside parent cub
	void Start() {
		cubeMap = GameObject.Find("CubeGenerator").gameObject.GetComponent<CubeMap>();
		originalPos = transform.localPosition;
		cubeCtrl = transform.root.gameObject.GetComponent<CubeController>();
	}

	public void landing() {
		cubeCtrl.isLand = true;
	}
	
	public void toOriginalPos() {
		transform.localPosition = originalPos;
	}
	
	public void resetOriginalPos() {
		originalPos = transform.localPosition;
	}
	

	public Vector2 cubePos() {
		return transform.position;
	}

	public void recorrectRotate() {
		transform.Rotate (new Vector3(transform.rotation.x, transform.rotation.y, Mathf.Round(-90f)));
	}

	public void CallCubeGenerator() {
		cubeMap.generateCube();
	}
	
	//Fix cube position after collision
	void OnCollisionEnter2D(Collision2D collision) {
		if (!rigidbody2D.isKinematic) {
			if ( ( collision.gameObject.tag == "Floor" || collision.gameObject.tag == "Cubes" )
		    && collision.transform.parent != transform.parent ) {
				cubeHitFloor();
			}
			if (gameObject.layer==2 && collision.gameObject.tag == "Player" &&
				 collision.gameObject.GetComponent<PlayerController>().carryCube) {
				cubeHitFloor();
			}
		}
	}//End of collision
	
	private void cubeHitFloor() {
		cubeCtrl.fixCube(true);
		
		if (cubeCtrl.isLand == false) {
			landing();
			CallCubeGenerator();
		}
	}
}
