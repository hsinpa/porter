﻿using UnityEngine;
using System.Collections;
using SocketIO;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using My_Utiltiy;

public class SocketI0_Game : MonoBehaviour {
	private SocketIOComponent socket;
	private int staticWidth = 720;
	private GameObject opponentHead;
	private GameObject opponentScore;
	private Animator opponentAnim;
	private string enemyData;
	private GameOverManager gameOverMM;
	private bool skinUpdate = false;
	private Sprite[] skinSet;
	
	public GameObject progressBar;
	public GameObject gameOver;

	ArrayList progressBarArray = new ArrayList();
	void Start () {
		progressBar.SetActive(true);
		opponentHead = progressBar.transform.FindChild("OpponentHead").gameObject;
		opponentScore = opponentHead.transform.FindChild("OpponentScore").gameObject;
		opponentAnim = progressBar.GetComponent<Animator>();		
		gameOverMM = gameOver.GetComponent<GameOverManager>();

		setProgressArray();
		socket = gameObject.GetComponent<SocketIOComponent>();
		socket.On("updateEnemyBar", updateEnemyBar);
		socket.On("queueUpIronCube", queueUpIronCube);
		socket.On("userDisconnect", userDisconnect);
		updateSkin(PlayerPrefs.GetString("character", "fireFighter"));
	}
	
	public void setProgressArray() {
		float screenPiece = staticWidth / 20;
		float rootWidth = 0 - staticWidth/2;
		
		for (int i = 0; i < 19; i++ ) {
			rootWidth += screenPiece;
			progressBarArray.Add(rootWidth);
		}
		opponentHead.GetComponent<RectTransform>().anchoredPosition = new Vector2((float)progressBarArray[0] ,0);
	}
	/** =============================== ON Socket Listener ================================ **/
	
	//update enemy score, position and avatar
	public void updateEnemyBar(SocketIOEvent e) {
		JSONObject obj = new JSONObject(e.data.ToString());
		opponentHead.GetComponent<RectTransform>().anchoredPosition = new Vector2((float)progressBarArray[Int32.Parse(obj.GetField("heightIndex").str)] ,0);
		opponentScore.GetComponent<Text>().text = obj.GetField("name").str +"\n"+ obj.GetField("score").str;
		
		enemyData = e.data.ToString();
	}
	
	public void queueUpIronCube(SocketIOEvent e) {
		CubeGenerator.ironQueue = true;
		opponentAnim.Play("EnemyHappy");
	}
	
	void userDisconnect(SocketIOEvent e) {
		prepareSaveDataBeforeClose();
		gameOverMM.gameOver();
	}
	/** =============================== Call Socket Function ================================ **/
	
	//Update Player's Cube Height and Score
	public void updateStatus(string index) {
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["score"] = ScoreManager.score.ToString();
		data["heightIndex"] = index;
		data["name"] = PlayerPrefs.GetString("name", "Anonymous");
		data["character"] = CubeMap.characterPrefab;
		data["facebook_id"] = SocketIO_Room.game_id;
		data["overAllScore"] = GameOverManager.getOverAllScore().ToString();
		
		socket.Emit("updateStatus", new JSONObject(data));
	}
	
	//Send Iron Cube to component
	public void ironCubeQueue() {
		socket.Emit("ironCubeQueue");
		opponentAnim.Play("EnemyCry");
	}
	/** =============================== OTHERS Function ================================ **/
	
	//Change Enemy Skin
	public void updateSkin(string name) {
		if (!skinUpdate) {
			skinUpdate = true;
			Transform hand = opponentHead.transform.FindChild("OpponentHand");
			Transform weapon = hand.FindChild("OpponentWeapon");
			
			skinSet = Resources.LoadAll<Sprite>("gameSprite/Player/Character/"+name);
			//opponentHead.GetComponent<Image>().sprite = skinSet[Array.IndexOf(skinSet, name+"_head")];
			foreach(Sprite skin in skinSet) {
			//	Debug.Log(skin.name);
				if (skin.name.Equals(name+"_head")) opponentHead.GetComponent<Image>().sprite = skin;
				if (skin.name.Equals(name+"_hand")) hand.GetComponent<Image>().sprite = skin;
				if (skin.name.Equals(name+"_weapon")) weapon.GetComponent<Image>().sprite = skin;	
			}
		}
	}
	
	//Send players data and disconnect
	public void prepareSaveDataBeforeClose() {
		JSONObject playerData = new JSONObject();
		playerData.AddField("score", ScoreManager.score.ToString());
		playerData.AddField("overAllScore", GameOverManager.getOverAllScore().ToString());
		playerData.AddField("name", PlayerPrefs.GetString("name", "Anonymous"));
		playerData.AddField("stage", CubeMap.themePrefab);
		playerData.AddField("character", CubeMap.themePrefab);
		playerData.AddField("time", Time.time - CubeMap.start_time);
		playerData.AddField("stage", CubeMap.themePrefab);
		playerData.AddField("facebook_id", SocketIO_Room.game_id);
		
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["playerOne"] = playerData.ToString().Replace("\"", "'");
		data["playerTwo"] = enemyData.Replace("\"", "'");
		socket.Emit("saveMatch", new JSONObject(data));
		socket.Close();
	}
}
