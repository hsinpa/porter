﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SocketIO;
using UnityEngine.UI;
using My_Utiltiy;

public class SocketIO_Room : MonoBehaviour {
	public SocketIOComponent socket;
	public static string game_id;
	//Scene Gameobject
	public GameObject gameScene;
	public GameObject multiplayerScene;
		
	public GameObject friendPanel;
	public GameObject friendContainer;
	public GameObject friendInvitePanel;
	
	private ScrollableList scrollableList;
	private LocalizationManager locationManager;
	
	private GameObject searchRoomButton;
	private GameObject playWithFriendButton;
	private GameObject userMessage;
	private SocketI0_Game socket_game;
	private string inviter_id;
	private string inviterSocket_id;
	
	private string room_id;
	public string socket_id;
	
	// Use this for initialization
	void Start () {
		game_id = "";
		socket = gameObject.GetComponent<SocketIOComponent>();
		socket.On("askUserInfo", loginInfo);	
		socket.On("onlineFriend", getOnlineFriends);
		socket.On("prepareGame", prepareGame);
		socket.On("inviteByFBFriend", inviteByFBFriend);
		
		scrollableList = friendContainer.GetComponent<ScrollableList>();
		scrollableList.socketRoom = this;
		socket_game = gameObject.GetComponent<SocketI0_Game>();
		searchRoomButton = GameObject.Find("MultiplayerMenu/Canvas/SearchRoom");
		playWithFriendButton = GameObject.Find("MultiplayerMenu/Canvas/SearchFriend");
		userMessage = GameObject.Find("MultiplayerMenu/Canvas/Message");
		locationManager = GameObject.Find("LocalizationManager").GetComponent<LocalizationManager>();		
		setTitle();
	}
	
	//=================================    Socket IO Hanlder    =================================
	void loginInfo(SocketIOEvent e) {
		JSONObject obj = new JSONObject(e.data.ToString());
		searchRoomButton.GetComponent<Button>().enabled = true;
		playWithFriendButton.GetComponent<Button>().enabled = true;
		userMessage.GetComponentInChildren<Text>().text = PlayerPrefs.GetString("name", "Anonymous") + "\n Online Users: " + obj.GetField("onlineNum").n;
		socket_id = obj.GetField("socket_id").str;
		game_id = (!PlayerPrefs.GetString("facebookId", "").Equals("")) ? PlayerPrefs.GetString("facebookId") : PlayerPrefs.GetString("uuid");
		
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["id"] = game_id;
		data["first_name"] = PlayerPrefs.GetString("name", "Anonymous");
		data["friends"] = PlayerPrefs.GetString("friends").Replace("\"", "'");	
		socket.Emit("loginInfo", new JSONObject(data));
	}
	
	void getOnlineFriends(SocketIOEvent e) {
		JSONObject obj = new JSONObject(e.data.ToString());
		scrollableList.generateFriendList(obj);
	}
	
	void prepareGame(SocketIOEvent e) {
		JSONObject obj = new JSONObject(e.data.ToString());
		CubeMap.themePrefab = obj.GetField("stage").str;
		multiplayerScene.SetActive(false);
		gameScene.SetActive(true);
		
		socket.Emit("gameStart");
	}

	void inviteByFBFriend(SocketIOEvent e) {
		if ( GameObject.Find("GameScene") == null ) {
			Debug.Log ("Not in Game");
			friendInvitePanel.SetActive(true);
			JSONObject obj = new JSONObject(e.data.ToString());
			inviter_id = obj.GetField("sender_id").str;
			room_id = obj.GetField("room_id").str;
			inviterSocket_id = obj.GetField("inviterSocket_id").str;
			
			friendInvitePanel.transform.FindChild("competitorName").GetComponent<Text>().text = "Start Match With " + obj.GetField("name").str;
		}
	}
	
	//=================================    Game Interface Function    =================================
	private void setTitle() {
		GameObject.Find("MultiplayerMenu/Canvas/Back/Text").GetComponent<Text>().text = locationManager.GetWord("back");
		GameObject.Find("MultiplayerMenu/Canvas/SearchRoom/Text").GetComponent<Text>().text = locationManager.GetWord("searchPlayer");
		GameObject.Find("MultiplayerMenu/Canvas/SearchFriend/Text").GetComponent<Text>().text = locationManager.GetWord("findFriend");
		GameObject.Find("MultiplayerMenu/Canvas/Message").GetComponent<Text>().text = locationManager.GetWord("waitConnection");
		GameObject.Find("MultiplayerMenu/Canvas/Title").GetComponent<Text>().text = locationManager.GetWord("multiPlayer");
	}
	
	
	public void backToMainScene() {
		socket.Close();
		Application.LoadLevel("menuScene");
	}
	
	public void searchPlayer() {
		socket.Emit("searchRoom");
		searchRoomButton.GetComponent<Button>().interactable = false;
		searchRoomButton.GetComponentInChildren<Text>().text = "Searching . . .";	
	}
		
	public void findOnlineFriends() {
		friendPanel.SetActive(true);
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["friends"] = PlayerPrefs.GetString("friends", "").Replace("\"", "'");
		socket.Emit("findOnlineFriends",  new JSONObject(data));
	}
	
	public void closeFriendsPanel() {
		friendPanel.SetActive(false);
	}
	
	//FB INVITATION
	public void inviteFBFriend(string facebook_id) {
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["invite_id"] = facebook_id;
		data["id"] = PlayerPrefs.GetString("facebookId", "null");
		
		data["name"] = PlayerPrefs.GetString("name", "Anonymous");
		socket.Emit("inviteFriend", new JSONObject(data));
	}
	
	public void inviteFriendResult(string answer) {
		
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["answer"] = answer;
		data["inviter_id"] = inviter_id;
		data["senderSocket_id"] = inviterSocket_id;
		data["room_id"] = room_id;
		
		socket.Emit("inviteFriendResult",  new JSONObject(data));
		
		friendInvitePanel.SetActive(false);
	}
	//END OF FB INVIVATION
}
