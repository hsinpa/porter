﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {
	
	public ParticleSystem particleEffect;
	private AudioSource audioSource;
	
	private GameObject player;
	private GameObject fireTrap;
	private CubeMap cubeMap;
	
	private int envirChangeTime = 150;
	private float difficultyUp = 1.2f;
	
	// Use this for initialization
	void Start () {
		cubeMap = gameObject.GetComponent<CubeMap>();
		audioSource = gameObject.GetComponentsInChildren<AudioSource>()[1];
		fireTrap = (GameObject)Resources.Load("prefab/fireTrap", typeof(GameObject));
		InvokeRepeating("environmentChange", envirChangeTime, envirChangeTime);
		
		player = GameObject.FindGameObjectWithTag("Player");
		
	}
	
	//=============================  Sky Trap  ===============================
	public IEnumerator projectTrap() {
		yield return new WaitForSeconds(4);
		
		float projectPos = CubeMap.generatePosition(),
		dropPercentage = Random.Range(0, 10) * 0.1f;
		if (dropPercentage <= 0.2f) {
			Instantiate (fireTrap, new Vector2 (projectPos + 0.5f, transform.position.y), fireTrap.transform.rotation);
		}
	}
	
	//=============================  Environment Change  ==============================
	//Slower Player's move speed
	void environmentChange() {
		particleEffect.Play();
		audioSource.clip = Resources.Load<AudioClip>("MovementMusic/rain");
		//audioSource.Play();
		setPlayerStatus(true);
		StartCoroutine(stopDebuff());
	}
	
	IEnumerator stopDebuff() {
		yield return new WaitForSeconds(16f);
		particleEffect.Stop();
		setPlayerStatus(false);
		cubeMap.gravity += difficultyUp;
		audioSource.Stop();
	}
	
	void setPlayerStatus(bool debuff) {
		float carrySpeed = player.GetComponent<PlayerController>().carrySpeed;
		player.GetComponent<PlayerController>().carrySpeed = (debuff) ? carrySpeed / 1.8f : carrySpeed * 1.8f;
		float moveSpeed = player.GetComponent<PlayerController>().moverSpeed;
		player.GetComponent<PlayerController>().moverSpeed = (debuff) ? moveSpeed / 1.8f : moveSpeed * 1.8f;
		
	}
}
