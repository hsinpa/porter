﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CubeMap : MonoBehaviour {
	//Grids
	private static List<float> cubePosition = new List<float>();
	public static bool multiplayerMode;
	public static float start_time;
	public static string characterPrefab;
	public static string themePrefab = "Night_Theme";
	public static string baseUrl = "http://porter-hsinpa.rhcloud.com/";
	//public static string baseUrl = "http://localhost:3000/";
	
	public float gravity = 10;
	
	//GameObject
	public GameObject floor;
	public GameObject leftWall;
	public GameObject rightWall;
	public GameObject raycastCubeY;
	public GameObject raycastCubeX;
	public GameObject characterModel;
	public GameObject backgroundModel;
	public List<RaycastX> raycastXSet= new List<RaycastX>();
	public List<RaycastY> raycastYSet= new List<RaycastY>();
	
	//Screen Scaling
	public float screenSizeX;
	public float screenSizeY;
	public float floorPos;
	Camera cam;

	//Include Scripts
	CubeGenerator cubegenerator;

	// Basic Map Scaling before game start
	void Awake () {
		multiplayerMode = (GameObject.Find("SocketIO") != null) ? true : false;
		characterPrefab = PlayerPrefs.GetString("character", "fireFighter");
		
		start_time = Time.time;
		Time.timeScale = 1;
		cam = Camera.main;
		//Get Camera Size
		screenSizeY = 2f * cam.orthographicSize;
		screenSizeX = screenSizeY * cam.aspect;
		
		cubePosition.Clear();
		calculateCubePos ();
		createStickFloor (cam);
		createStickLeftWall (cam);
		createStickRightWall (cam);
		loadTheme(themePrefab);
		createObstacle();
		loadCharacter();
		loadRaycast();
		
		Application.targetFrameRate = 60;
	}

	//Start the game after 1 seconds
	void Start() {
		cubegenerator = GetComponent<CubeGenerator>();
		generateCube ();
	}

	//============================================ Setting Game Environment ===============================
	//Load Background
	private void loadTheme(string theme) {
		GameObject loadTheme = (GameObject)Resources.Load("prefab/"+theme);
		Instantiate (loadTheme, new Vector2 (-2, 13), transform.rotation);		
	}
	
	private void loadCharacter() {
		
		GameObject character = (GameObject)Resources.Load("gameSprite/Player/" + characterPrefab);
		Transform spawn = Instantiate (character.transform, new Vector2 (cubePosition[5], floorPos+1.3f), transform.rotation) as Transform;		
		spawn.transform.parent = transform.parent;
	}
	
	private void loadRaycast() {
		GameObject[] raycastX = GameObject.FindGameObjectsWithTag ("RaycastX");
		GameObject[] raycastY = GameObject.FindGameObjectsWithTag ("RaycastY");
		foreach (GameObject go in raycastX) {
			raycastXSet.Add(go.GetComponent<RaycastX>());
		}
		foreach (GameObject go in raycastY) {
			raycastYSet.Add(go.GetComponent<RaycastY>());
		}
	}
	
	//Create Wook Obstacle during night theme
	void createObstacle() {
		if (themePrefab.Equals("Night_Theme") ) {
			GameObject plankPref = (GameObject)Resources.Load("gameSprite/Plank");
			GameObject plankLeft = Instantiate (plankPref.transform, new Vector2 (cubePosition[1], floorPos+3), transform.rotation) as GameObject;		
			GameObject plankRight = Instantiate (plankPref.transform, new Vector2 (cubePosition[10], floorPos+3), transform.rotation) as GameObject;		
		}
	}

	//Fix the game width to 13 grids
	void calculateCubePos() {
		float coloumnStartPoint;
		coloumnStartPoint = -5.5f;
		cubePosition.Add(coloumnStartPoint);
		
		for (int i = 0; i < 10; i++) {
			coloumnStartPoint += 1;

			cubePosition.Add(coloumnStartPoint);
		}
	}

	void createStickFloor(Camera cam) {
		
		floorPos = (screenSizeY / 2f);
		floorPos = (-floorPos) - 0.5f;
		floor.transform.position = new Vector2 (0f, floorPos);
	}

	//Stick leftwall and raycastDetectors
	void createStickLeftWall(Camera cam) {
		float leftWallPos = cubePosition [0] - 0.5f;
		leftWall.transform.position = new Vector2 (leftWallPos, 0);

		//For RayCast Detector's position
		raycastCubeY.transform.position = new Vector2 (leftWallPos - 1.05f, floorPos +1f);
		raycastCubeX.transform.position = new Vector2 (leftWallPos + 1f, floorPos + 0.05f);

	}

	void createStickRightWall(Camera cam) {
		float rightWallPos = cubePosition[10] + 1.5f;
		rightWall.transform.position = new Vector2 (rightWallPos, 0f);
	}
	
	//============================================ Simple Game Control Order ================================
	
	static public float generatePosition() {
		int cubePosLength = cubePosition.Count;
		return cubePosition[Random.Range(1, cubePosLength-1)] + 0.5f;
	}
	
	//Game start
	public void generateCube() {		
		cubegenerator.createCube (generatePosition());
	}
}