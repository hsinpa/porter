﻿using UnityEngine;
using System.Collections;

public class RaycastY : MonoBehaviour {
	
	public GameObject gameOver;
	public int cubeIndex;
	private float lastCallTime = 0f;

	int cubeLayer = 1 << 9;
	int CombinedMask;
    int callAmount = 0;
	
	GameOverManager gameOverMM;
	SetRayCastPosition headRaycastY;
	ScoreManager scoreManager;
	
	void Start() {
		gameOverMM = gameOver.GetComponent<GameOverManager>();
		headRaycastY = transform.parent.GetComponent<SetRayCastPosition>();
		scoreManager = GameObject.Find("GameScene/Canvas/Score").GetComponent<ScoreManager>();
	}
	
//	void Update() {
//		Debug.DrawLine(transform.position, new Vector3(transform.position.x+15, transform.position.y , transform.position.z), Color.red);
//	}

	public void raycastYPos(bool isDelete) {
		if (Time.time > lastCallTime) {
		
			lastCallTime = Time.time + 0.0001f;
			CombinedMask = cubeLayer;

		
		RaycastHit2D[] hits;
		hits = Physics2D.CircleCastAll (transform.position, 0.028f, new Vector2 (1f, 0f), 15.0F, CombinedMask);
		float hitHeight;
		int i = 0;
		raycastYrepos(hits, isDelete);
		
		//Game Over
		if (transform.name ==  "RayCastLastDetect" && hits.Length > 0 && isDelete) {
			Debug.Log("Game Over");
			gameOverMM.gameOver();
			return;
		}
			if (hits.Length > 0 ) {
				pushCubeHeight();
			}
		
			//Delete a cube row
			if (hits.Length >= 11 && isDelete) {
				hitHeight = hits[0].transform.position.y;
				
				while (i < hits.Length) {
					RaycastHit2D hit = hits [i];
					Destroy (hit.collider.gameObject);
					i++;
				}
				
				StartCoroutine(releaseGravity(hitHeight));
				scoreManager.addScore();
				
			}
		}
	}
	
	
	IEnumerator releaseGravity(float hitHeight) {
		GameObject[] gos;
		yield return new WaitForSeconds(0.05f);
		gos = GameObject.FindGameObjectsWithTag("Cubes");
		
		foreach (GameObject go in gos) {
			if (go.transform.position.y >= hitHeight && (go.layer == 9 )) {
				go.rigidbody2D.isKinematic = false;
				go.rigidbody2D.velocity = new Vector2(0, -8f);
			}
		}
	}
	
	//use to reposition the fallen cube in paralle to raycastDetector
	void raycastYrepos(RaycastHit2D[] hits, bool isDelete) {
		int i = 0;
		while (i < hits.Length) {
				RaycastHit2D hit = hits [i];
			hit.collider.gameObject.transform.position = new Vector2(hit.collider.gameObject.transform.position.x,
			                                                         transform.position.y);
			if (isDelete) {
					hit.collider.rigidbody2D.isKinematic = true;
			}
				i++;
		}
	}
	
	void pushCubeHeight() {
		headRaycastY.cubeHeightArray.Add(cubeIndex);
	}
}
