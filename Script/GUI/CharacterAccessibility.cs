﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterAccessibility : MonoBehaviour {
	public JSONObject availableCharacter;
	

	public bool checkCharacterPossibility(string name){
		availableCharacter = new JSONObject(PlayerPrefs.GetString("availableCharacter", loadDefaultCharacter()));
		return (availableCharacter.GetField(name).str.Equals("true")) ? true : false;
	}
	
	public void unlockCharacter(string name) {
		availableCharacter.RemoveField(name);
		availableCharacter.AddField(name, "true");
		PlayerPrefs.SetString("availableCharacter", availableCharacter.ToString());
	}
	
	private string loadDefaultCharacter() {
		JSONObject json = new JSONObject();
		json.AddField("police", "true");
		json.AddField("fireFighter", "true");
		json.AddField("worker", "false");
		return json.ToString();
	}
}
