﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using FacebookManager;
using UnityEngine.UI;

public class mainMenuEventManager : MonoBehaviour {
	private facebookMain facebookMain;
	private AdsManager adsManager;
	private static GameObject welcomeStatement;
	private static GameObject facebookButton;
	private GameObject creditPanel;
	
	public static LocalizationManager locationManager;
		
	void Start() {
		//PlayerPrefs.DeleteAll();
		FB.Init(SetInit, OnHideUnity);
		adsManager = gameObject.AddComponent<AdsManager>();
		facebookMain = gameObject.AddComponent<facebookMain>();
		facebookButton = GameObject.Find("Canvas/fbLogin");
		welcomeStatement = GameObject.Find("Canvas/WelcomeStatement");
		creditPanel = GameObject.Find("Canvas/CreditPanel");
		locationManager = GameObject.Find("LocalizationManager").GetComponent<LocalizationManager>();
		
		updateUIStatus(PlayerPrefs.GetString("name", ""));
		setUUID();
		setTitle();
		adsManager.requestUnityAds();
		AdsManager.RequestBanner();
	}
	
	private void setTitle() {
		GameObject.Find("Canvas/Soloplay/Text").GetComponent<Text>().text = locationManager.GetWord("singlePlayer");
		GameObject.Find("Canvas/Multiplay/Text").GetComponent<Text>().text = locationManager.GetWord("multiPlayer");
	}
	
	public void selectLanguage(string lang) {
		locationManager.SetLang(lang);
		GameObject.Find("Canvas/CreditPanel/EN/Text").GetComponent<Text>().color = Color.black;
		GameObject.Find("Canvas/CreditPanel/CN/Text").GetComponent<Text>().color = Color.black;
		GameObject.Find("Canvas/CreditPanel/"+lang+"/Text").GetComponent<Text>().color = Color.yellow;
		setTitle();
	}
	
	public void startSinglePlayer() {
		Application.LoadLevel("ChooseCharacter");
	}
	
	
	public void startMultiPlayer() {
		Application.LoadLevel("MultiplayerScene");
	}
	
	public void facebookConnect() {
		if (PlayerPrefs.GetString("name", "").Equals("")) {
			facebookMain.facebookLogin();
		} else {
			facebookMain.facebookLogout();
		}
	}
	
	public void showHighScore() {
		Application.LoadLevel("highScore");
	}
	
	public void creditHandler(bool command) {
		if (command) {
			creditPanel.GetComponent<Animator>().SetBool("Show", true);
		} else {
			creditPanel.GetComponent<Animator>().SetBool("Show", false);
		}
	}
	
	static public void updateUIStatus(string fbName) {
		Sprite fbImage;
		if (facebookButton != null) {
			if (!PlayerPrefs.GetString("name", "").Equals("")) {
				fbImage = Resources.Load<Sprite>("MenuPage/FBLogout");
				facebookButton.GetComponent<Image>().sprite = fbImage;
			} else {
				fbImage = Resources.Load<Sprite>("MenuPage/FBConnect");
				facebookButton.GetComponent<Image>().sprite = fbImage;
				
			}
			welcomeStatement.GetComponent<Text>().text = fbName;
		}	
	}
	
	private void setUUID() {
		if (PlayerPrefs.GetString("facebookId", "").Equals("")) {
			PlayerPrefs.SetString("facebookId", Guid.NewGuid().ToString());
		}
		string daymatch = PlayerPrefs.GetString("matchDay_Theme", "");
		string nightmatch = PlayerPrefs.GetString("matchNight_Theme", "");
		
		if (!daymatch.Equals("")) 
			sendToServer(new JSONObject(daymatch));
		if (!nightmatch.Equals("")) 
			sendToServer(new JSONObject(nightmatch));
	}
	
	private void sendToServer(JSONObject obj ) {
		WWWForm form = new WWWForm();
		List<string> keys = obj.keys;
		for(int i = 0; i < obj.list.Count; i++) {
			string key = keys[i];
			string val = obj.list[i].ToString();
			val = val.Replace("\"", "");
			form.AddField(key, val);
		}
		WWW www = new WWW(CubeMap.baseUrl + "saveRecord", form);
	}
	
	
	private void SetInit() {
		enabled = true; 
		// "enabled" is a magic global; this lets us wait for FB before we start rendering
	}
	
	private void OnHideUnity(bool isGameShown) {
		if (!isGameShown) {
			// pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// start the game back up - we're getting focus again
			Time.timeScale = 1;
		}
	}
}
