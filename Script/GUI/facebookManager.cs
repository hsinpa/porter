﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook;


namespace FacebookManager {
public class facebookMain : MonoBehaviour {
	
		public void facebookLogin() {
			FB.Login("user_friends", AuthCallback);
		}
		
		public void facebookLogout() {
			FB.Logout();
			PlayerPrefs.DeleteAll();
			mainMenuEventManager.updateUIStatus("");
		}
		
		void showUserData(string JSONData) {
			string past_id = PlayerPrefs.GetString("facebookId");
			
			JSONObject json = new JSONObject(JSONData);
			PlayerPrefs.SetString("facebookId", json.GetField("id").str);			
			PlayerPrefs.SetString("name", json.GetField("first_name").str);
						
			JSONObject friendsData= (JSONObject)json.GetField("friends");
			PlayerPrefs.SetString("friends", friendsData.GetField("data").ToString());
			
			WWWForm form = new WWWForm();
			form.AddField("past_id", past_id);
			form.AddField("new_id", json.GetField("id").str);
			form.AddField("name", json.GetField("first_name").str);
			
			StartCoroutine("updatePlayerPref", form);
						
			mainMenuEventManager.updateUIStatus(PlayerPrefs.GetString("name"));
		}
		
		//Get User's Previous Data
		IEnumerator updatePlayerPref(WWWForm form) {
			WWW www = new WWW(CubeMap.baseUrl + "updateFBID", form);
			yield return www;
			
			if (string.IsNullOrEmpty(www.error)) {
				JSONObject json = new JSONObject(www.text);
				Debug.Log (www.text);
								
				PlayerPrefs.SetString("matchDay_Theme", json.GetField("getDayHighScore").ToString());			
				PlayerPrefs.SetString("matchNight_Theme", json.GetField("getNightHighScore").ToString());
			}
		}
		
		//======================================= FB Function =====================================
		void AuthCallback(FBResult result) {
			if(FB.IsLoggedIn) {
				OnLoggedIn();
			} else {
				Debug.Log("User cancelled login");
			}
		}
		
		public void OnLoggedIn()
		{		
			// Reqest player info and profile picture                                                                           
			FB.API("/me?fields=id,first_name,friends", Facebook.HttpMethod.GET, APICallback);  
		}
		
		void APICallback(FBResult result)                                                                                              
		{                                                                                                                              
			if (result.Error != null)                                                                                                  
			{                                                                                                                    
				Debug.LogError(result.Error);                                                                                           
				// Let's just try again
				FB.API("/me?fields=id,first_name,friends", Facebook.HttpMethod.GET, APICallback);  
				return;
			}
			//Debug.Log(result.Text);                                                                                              
			showUserData(result.Text);                                                               
		}
	}//end of class
}
