﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HightScoreManager : MonoBehaviour {
	private Text onlineMatchScore;
	private Text playerDayScore;
	private Text playerNightScore;
	private Text topDayScore;
	private Text topNightScore;
	private JSONObject storeRecord;
	
	// Display the last record data first, and show the new data if internet is connected
	void Start () {
		onlineMatchScore = GameObject.Find("Canvas/onlineMatchScore").GetComponent<Text>();
		playerDayScore = GameObject.Find("Canvas/playerDayScore").GetComponent<Text>();
		playerNightScore = GameObject.Find("Canvas/playerNightScore").GetComponent<Text>();
		topDayScore = GameObject.Find("Canvas/Panel/topDayScore").GetComponent<Text>();
		topNightScore = GameObject.Find("Canvas/Panel/topNightScore").GetComponent<Text>();
		showScore(PlayerPrefs.GetString("highScore", ""));
		
		//call server
		WWWForm form = new WWWForm();
		form.AddField("id", PlayerPrefs.GetString("facebookId", ""));
		StartCoroutine("getScoreFromWeb", form); 
	}
	
	IEnumerator getScoreFromWeb(WWWForm form) {
		WWW www = new WWW(CubeMap.baseUrl + "getTopTen", form);
		yield return www;
		Debug.Log (www.text);
		if (string.IsNullOrEmpty(www.error)) {
			showScore(www.text.ToString());
			PlayerPrefs.SetString("highScore",www.text);
			PlayerPrefs.Save();
		}
	}
	
	//Display Score
	void showScore(string jsonString) {
		if (!PlayerPrefs.GetString("matchDay_Theme").Equals("") && !PlayerPrefs.GetString("matchNight_Theme").Equals("")) {
			playerDayScore.text = new JSONObject(PlayerPrefs.GetString("matchDay_Theme")).GetField("overAllScore").n.ToString();
			playerNightScore.text = new JSONObject(PlayerPrefs.GetString("matchNight_Theme")).GetField("overAllScore").n.ToString();
		}
		
		if (jsonString.Length > 0) {
			storeRecord = new JSONObject(jsonString);
			onlineMatchScore.text = storeRecord.GetField("win") + " : "+ storeRecord.GetField("lose");
			
			//for top20
			showTopScore (storeRecord.GetField("day"), topDayScore);
			showTopScore (storeRecord.GetField("night"), topNightScore);
			
		}
	}
	
	//Display Score for Top 20
	void showTopScore(JSONObject json, Text themeRow) {
		themeRow.text = "";
		int i = 1;
		foreach(JSONObject j in json.list){
			themeRow.text += i+". " + j.GetField("name").str + " " + j.GetField("overAllScore").n +"\n";
			i++;
		}		
	}
	
	//BACK
	public void backToMainPage() {
		Application.LoadLevel("menuScene");
	}
}
