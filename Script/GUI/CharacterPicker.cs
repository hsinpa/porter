﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class CharacterPicker : MonoBehaviour {
	private CharacterAccessibility characterAccessibility;
	private Image characterImage;
	private Text characterDescription;
	private Sprite characterSprite;
	private Image background; 
	
	public GameObject videoPanel;
	private GameObject unlockButton;
	private GameObject startButton;
	private LocalizationManager locationManager;
	
	private int currentIndex = 0;
	// Use this for initialization
	void Start () {
		characterAccessibility = gameObject.AddComponent<CharacterAccessibility>();
		background = GameObject.Find("Canvas/Background").GetComponent<Image>();
		startButton = GameObject.Find("Canvas/Panel/Start");
		unlockButton = GameObject.Find("Canvas/Panel/unLock");
		locationManager = GameObject.Find("LocalizationManager").GetComponent<LocalizationManager>();		
		characterImage = gameObject.GetComponentsInChildren<Image>()[1];
		
		characterDescription = gameObject.GetComponentInChildren<Text>();
		
		pickMap(PlayerPrefs.GetString("theme", "Day"));
		pickCharacter(PlayerPrefs.GetString("character", "fireFighter"));
	}
	
	public void pickMap(string theme) {
		CubeMap.themePrefab = theme + "_Theme";
		string closeTheme = (theme.Equals("Day")) ? "Night" : "Day";
		GameObject.Find("Canvas/"+ theme +"/Text").GetComponent<Text>().color =  Color.yellow;
		GameObject.Find("Canvas/"+ closeTheme +"/Text").GetComponent<Text>().color =  Color.white;
		PlayerPrefs.SetString("theme", theme);
	}
	
	public void pickCharacter(string name) {
		changeImage(name);
		PlayerPrefs.SetString("character", name);
		characterDescription.text = locationManager.GetWord(name+"Desc");
		if (characterAccessibility.checkCharacterPossibility(name)) {
			startButton.SetActive(true);
			unlockButton.SetActive(false);
		} else {
			startButton.SetActive(false);
			unlockButton.SetActive(true);
		}
	}
	
	public void watchVideo() {
		if (Advertisement.isReady()) {
			ShowOptions options = new ShowOptions();
			options.pause = true;                        // Pauses game while ads are shown
			options.resultCallback = finishVideo;   // Triggered when the ad is closed
			Advertisement.Show("defaultVideoAndPictureZone", options);			
		}
	}
	private void finishVideo(ShowResult result) {
		closeVideoPanel();
		characterAccessibility.unlockCharacter(PlayerPrefs.GetString("character"));
		pickCharacter(PlayerPrefs.GetString("character"));
	}
	
	public void closeVideoPanel() {
		videoPanel.SetActive(false);
	}
	
	public void showVideoPanel() {
		videoPanel.SetActive(true);
	}
	
	public void showTutorial() {
		GameObject.Find("Canvas/tutorialPage").GetComponent<Image>().enabled = true;
	}
	
	public void startGame() {
		Application.LoadLevel("gameScene");
	}
	
	public void fallBack() {
		Application.LoadLevel("menuScene");
	}
	
	private void changeImage(string name) {
		characterSprite =  Resources.Load<Sprite>("characterImage/" + name);
		characterImage.sprite = characterSprite;
		
	}
}
