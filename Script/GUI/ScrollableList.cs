﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ScrollableList : MonoBehaviour
{
    public GameObject itemPrefab;
    public int columnCount = 1;
    public SocketIO_Room socketRoom;
    
	
	public void generateFriendList(JSONObject friendList)
    {
		int itemCount = friendList.Count;
		RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
        RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();

        //calculate the width and height of each child item.
        float width = containerRectTransform.rect.width / columnCount;
        float ratio = width / rowRectTransform.rect.width;
        float height = rowRectTransform.rect.height * ratio;
        int rowCount = itemCount / columnCount;
        if (itemCount % rowCount > 0)
            rowCount++;

        //adjust the height of the container so that it will just barely fit all its children
        float scrollHeight = height * rowCount;
        containerRectTransform.offsetMin = new Vector2(containerRectTransform.offsetMin.x, -scrollHeight / 2);
        containerRectTransform.offsetMax = new Vector2(containerRectTransform.offsetMax.x, scrollHeight / 2);

        int j = 0;
        int i = 0;
		foreach(JSONObject friendRow in friendList.GetField("online").list){
			i++;
			
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
            if (i % columnCount == 0)
                j++;
            //create a new item, name it, and set the parent
            GameObject newItem = Instantiate(itemPrefab) as GameObject;
            
            newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
            newItem.transform.parent = gameObject.transform;

            //move and size the new item
            RectTransform rectTransform = newItem.GetComponent<RectTransform>();
			rectTransform.localScale = new Vector3(1, 1, 1);
			
            float x = -containerRectTransform.rect.width / 2 + width * (i % columnCount);
            float y = containerRectTransform.rect.height / 2 - height * j;
            rectTransform.offsetMin = new Vector2(x, y);

            x = rectTransform.offsetMin.x + width;
            y = rectTransform.offsetMin.y + height;
            rectTransform.offsetMax = new Vector2(x, y);
            
            //Replace Name and Status
			
			Text[] fInfo = newItem.GetComponentsInChildren<Text>();
			Button inviteButton = newItem.GetComponentInChildren<Button>();
			fInfo[0].text = friendRow.GetField("name").str;
			Image fProfile = newItem.transform.FindChild("profilePic").GetComponent<Image>();
			
			inviteButton.onClick.AddListener(() => socketRoom.inviteFBFriend(friendRow.GetField("id").str));
			StartCoroutine(loadProfileImage(friendRow.GetField("id").str, fProfile));
		}
	}
	
	
	public IEnumerator loadProfileImage(string userId, Image fProfile) {
		WWW url = new WWW("https://graph.facebook.com/" + userId + "/picture?type=large");
		yield return url;
		fProfile.sprite = Sprite.Create(url.texture, new Rect(0, 0, url.texture.width, url.texture.height), new Vector2(0.5f, 0.5f) );
		                  		
	}
	
	
}
